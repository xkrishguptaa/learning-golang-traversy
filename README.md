<div align="center">
  <img src="https://gitlab.com/xkrishguptaa/learning-golang-traversy/-/raw/main/logo.png" height="100px" width="100px" />
  <br />
  <h1>Learning Golang Traversy</h1>
  <p>Learning Golang from Traversy Media's Crash Course</p>
</div>

## 📖 Introduction

This is exactly the mirror of Traversy Media's Crash Course on Golang. I was trying to learn Go and this is my first project in Go.

Tutorial Link: [Golang Crash Course](https://www.youtube.com/watch?v=SqrbIlUwR0U)

## 📦 Built With

- [Go](https://golang.org/)

## 📖 Notes

### 📄 Types

```go
// ==============================
// Built-in Types
// ==============================
// string - "Hello, Gophers!" (%s)
// bool - true or false (%t)
// ------------------------------
// Integers
// ------------------------------
// int - -2147483648 to 2147483647 (%d)
// int8 - -128 to 127 (%d)
// int16 - -32768 to 32767 (%d)
// int32 - -2147483648 to 2147483647 (%d)
// int64 - -9223372036854775808 to 9223372036854775807 (%d)
// ------------------------------
// Unsigned Integers (only positive)
// ------------------------------
// uint - 0 to 4294967295 (%d)
// uint8 - 0 to 255 (%d)
// uint16 - 0 to 65535 (%d)
// uint32 - 0 to 4294967295 (%d)
// uint64 - 0 to 18446744073709551615 (%d)
// ------------------------------
// Aliases for int and uint
// ------------------------------
// byte - alias for uint8
// rune - alias for int32
// ------------------------------
// Floating Point Numbers
// ------------------------------
// float32 - IEEE-754 32-bit floating-point numbers (%f)
// float64 - IEEE-754 64-bit floating-point numbers (%f)
// complex64 - complex numbers with float32 real and imaginary parts (%f)
// complex128 - complex numbers with float64 real and imaginary parts (%f)
```

### 📄 Variables

```go
// ==============================
// Variables
// ==============================
// var <name> <type> // needs assignment later, explicit type
// var <name> <type> = <value> // mutable, explicit type
// ------------------------------
// const <name> <type> = <value> // immutable, explicit type
// const <name> = <value> // immutable, implicit type
// ------------------------------
// <name> := <value> // mutable, implicit type
// ------------------------------
// <name> = <value> // reassign value, only works with `var`
// ------------------------------
// <name1>, <name2> := <value1>, <value2> // multiple assignments
// ------------------------------
```

### 📄 Functions

```go
// ==============================
// Functions
// ==============================
// func <name>(<param> <type>, <param> <type>) <return type> { ... }
// ------------------------------
// func <name>(<param> <type>) (a <return type>, b <return type>) { ... }
```

### 📄 Importing Other Files

> Note: You can only import variables, functions, and types that start with a capital letter are public and in scope of the package

```go
// ==============================
// Importing Other Files
// ==============================
// import "<mod>/<path>" // import package from module (module is the name specified in go.mod)
//
// <path>.Name // access variable, function, or type
//
// <path>.<name> // DOES NOT WORK
```

### 📄 Arrays

> Arrays start at index 0

```go
// ==============================
// Arrays
// ==============================
// var <name> [<size>]<type> // mutable, explicit type
//
// <name> = [<size>]<type>{<value>, <value>, <value>} // assign values
//
//
// e.g.
// var a [5]int
// ------------------------------
// Accessing and Assigning Items
// ------------------------------
// <name>[<index>] // access item
// <name>[<index>] = <value> // assign item
// ------------------------------
// Appending Items
// ------------------------------
// <name> = append(<name>, <value>) // append item
// ------------------------------
// Deleting Items
// ------------------------------
// <name> = append(<name>[:<index>], <name>[<index>+1:]...) // delete item
```

> Tip: While defining variable, You can use `...` to let the compiler count the size for you

> Note: While defining variable, if <size> is not specified, it is unbounded

> Note: When you use `arr[0:3]` you get the elements including index 0 to index 3, excluding index 3 (so 0, 1, 2)

### 📄 Conditionals

```go
// ==============================
// Conditionals
// ==============================
// if <condition> { ... }
// ------------------------------
// if <condition> { ... } else { ... }
// ------------------------------
// if <condition> { ... } else if <condition> { ... } else { ... }
// ------------------------------
// switch <variable> {
//   case <value>: ...
//   case <value>: ...
//   default: ...
// }
```

### 📄 Loops

```go
// ==============================
// Loops
// ==============================
// for <variable> := <start>; <variable> < <end>; <variable>++ { ... }
// ------------------------------
// for <variable> < <end> { ... }
```

### 📄 Maps

> Maps are Key-Value pairs / Like JSON but only 1 level deep

```go
// ==============================
// Maps
// ==============================
// var <name> map[<key type>]<value type> // mutable, explicit type
//
// <name> = map[<key type>]<value type>{<key>: <value>, <key>: <value>, <key>: <value>} // assign values
//
// <name> = make(map[<key type>]<value type>) // create empty map
// ------------------------------
// Accessing and Assigning Items
// ------------------------------
// <name>[<key>] // access item
// <name>[<key>] = <value> // assign item (if key exists, it will be overwritten)
// ------------------------------
// Delete Items
// ------------------------------
// delete(<name>, <key>) // delete item
```

### 📄 Ranges

> Ranges are like for loops but for arrays, maps, and strings, etc.

```go
// ==============================
// Ranges
// ==============================
// for <index>, <value> := range <name> { ... }
// ------------------------------
// for _, <value> := range <name> { ... } // if you don't need the index
// ------------------------------ with maps
// for <key>, <value> := range <name> { ... }
```

### 📄 Pointers

> Pointers are like references in C++

```go
// ==============================
// Pointers
// ==============================
// * is used to declare a pointer
// & is used to get the address of a variable
// ------------------------------
// var <name> *<type> // mutable, explicit type
// ------------------------------
// <name> = &<variable> // assign address of variable
// ------------------------------
// *<name> // access value at address
```

### 📄 Structs

> Structs are like classes in C++

```go
// ==============================
// Structs
// ==============================
// type <name> struct {
//   <name> <type>
//   <name> <type> `json:"<name>" bson:"<name>" binding:"required"`
// }
// ------------------------------
// using struct
// ------------------------------
// var <name> <type> // mutable, explicit type
// ------------------------------
// <name> = <type>{<key>: <value>, <key>: <value>, <key>: <value>} // assign values
```

> Note: `json:"<name>" bson:"<name>" binding:"required"` is used for JSON, MongoDB, and Validation

### 📄 Interfaces

> Interfaces are like abstract classes in C++

```go
// ==============================
// Interfaces
// ==============================
// type <name> interface {
//   <name>(<param> <type>) <return type>
// }
```

> You can compare them to `extends` in TypeScript
