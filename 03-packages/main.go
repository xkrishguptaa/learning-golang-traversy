package main

import (
	"fmt"
	"learning-golang-traversy/03-packages/util"
	"math"
)

func main() {
	fmt.Println(math.Floor(2.7))
	fmt.Println(math.Ceil(2.7))
	fmt.Println(math.Sqrt(16))
	fmt.Println(util.Reverse("olleh"))
}
