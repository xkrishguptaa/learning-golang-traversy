package main

import "fmt"

func main() {
	var age uint8 = 30
	const isCool = true
	var size float32 = 2.3

	name, email := "Brad", "brad@gmail.com"

	fmt.Println(name, age, isCool, email)

	fmt.Printf("%T\n", size)
}
